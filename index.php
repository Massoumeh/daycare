<?php

require_once '_setup.php';

require_once '_main.php';
require_once '_users.php';
require_once '_admin.php';
require_once '_articles.php';

$app->run();
